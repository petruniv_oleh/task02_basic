package com.epam.numbers;

import java.util.Scanner;

/**
 * "Main" is the main class where everything starts..
 *
 * @author Oleh Petruniv
 */

public final class Main {
    /**
     * hidden constructor.
     */
    private Main() {
    }

    /**
     * <p>main method is the entrance point of the program.</p>
     *
     * @param args default parameters
     */
    public static void main(final String[] args) {

        int firstNumber;
        int lastNumber;
        int sizeOfFibonacciSet;
        int fibonacciF1;
        int fibonacciF2;
        Scanner input;
        input = new Scanner(System.in);
        System.out.println("Enter the begin of the interval");
        firstNumber = input.nextInt();
        System.out.println("Enter the end of the interval");
        lastNumber = input.nextInt();

        if (lastNumber % 2 == 0) {
            fibonacciF2 = lastNumber;
            fibonacciF1 = lastNumber - 1;
        } else {
            fibonacciF2 = lastNumber - 1;
            fibonacciF1 = lastNumber;
        }

        System.out.println("Enter the size of fibonacci set");
        sizeOfFibonacciSet = input.nextInt();

        Interval interval = new Interval(firstNumber, lastNumber);
        interval.printOddAndEven();
        interval.printSumOfOdd();
        interval.printSumOfEven();

        Fibonacci fibonacci = new Fibonacci(fibonacciF1, fibonacciF2,
                sizeOfFibonacciSet);
        fibonacci.percentageOfOddAndEven();

    }


}
