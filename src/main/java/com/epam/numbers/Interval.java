package com.epam.numbers;


/**
 * Interval is a class where we make interval of number
 * and do ome stuff with it..
 *
 * @author Oleh Petruniv
 */
public class Interval {
    /**
     * Interval begin numb.
     */
    private int beginOfInterval;
    /**
     * Interval end numb.
     */
    private int endOfInterval;
    /**
     * sum of odd numbers.
     */
    private int sumOdd;
    /**
     * sum of even numbers.
     */
    private int sumEven;

    /**
     * <p>In this constructor the variables are initialized.</p>
     *
     * @param intervalStart the first number of the interval
     * @param intervalEnd   the last number of the interval
     */
    public Interval(final int intervalStart, final int intervalEnd) {
        this.beginOfInterval = intervalStart;
        this.endOfInterval = intervalEnd;
        sumEven = 0;
        sumOdd = 0;
    }


    /**
     * <p>This method do next.</p>
     * <ul>
     * <li>print odd numbers from begin to end</li>
     * <li>print even numbers from end to begin</li>
     * <li>calculate sum of odd numbers</li>
     * <li>calculate sum of even numbers</li>
     * </ul>
     */
    public void printOddAndEven() {
        int begin;
        int end;

        begin = beginOfInterval;
        end = endOfInterval;
        System.out.println(" Odd");

        while (begin <= end) {
            if (begin % 2 != 0) {
                System.out.print("\t" + begin);
                sumOdd += begin;
            }
            begin++;
        }
        System.out.println("\n Even");
        begin = beginOfInterval;
        while (end >= begin) {
            if (end % 2 == 0) {
                System.out.print("\t" + end);
                sumEven += end;
            }
            end--;
        }
    }

    /**
     * <p>All it do is print the sum of odd numbers.</p>
     */
    public void printSumOfOdd() {
        System.out.println("\n\nSum of odd numbers: " + sumOdd);
    }

    /**
     * <p>All it do is print the sum of even numbers.</p>
     */
    public void printSumOfEven() {
        System.out.println("Sum of even numbers: " + sumEven);
    }
}
