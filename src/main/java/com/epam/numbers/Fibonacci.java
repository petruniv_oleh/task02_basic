/**
 * this is the main package.
 * @author Oleh Petruniv
 * @version 1.0
 */
package com.epam.numbers;

/**
 * Fibonacci is the class which can build a fibonacci set and also count
 * the percentage of odd and even.
 */
public class Fibonacci {
    /**
     * 100%.
     */
    private static final float PERCENTS = 100f;
    /**
     * an array for fibonacci set.
     */
    private int[] fibonacciSet;
    /**
     * a size of a set.
     */
    private int sizeOfSet;

    /**
     * <p>The constructor method for Fibonacci class. </p>
     * <p>here the main variables are initialized and also the calls the
     * {@link #buildSet(int size) buildSet} method</p>
     *
     * @param f1        is the first number of fibonacci set
     * @param f2        is the second number of fibonacci set
     * @param setSize is the size of fibonacci set
     */
    public Fibonacci(final int f1, final int f2, final int setSize) {
        this.sizeOfSet = setSize;
        fibonacciSet = new int[sizeOfSet];
        fibonacciSet[0] = f1;
        fibonacciSet[1] = f2;
        buildSet(sizeOfSet - 2);
    }

    /**
     * <p>This method is recursive and all he do is making a fibonacci
     * set number and put it into array.</p>
     *
     * @param size is the size of fibonacci set
     */
    private void buildSet(final int size) {
        if (size > 0) {
            int i;
            i = sizeOfSet - size;
            fibonacciSet[i] = fibonacciSet[i - 1] + fibonacciSet[i - 2];
            buildSet(size - 1);
        }
    }

    /**
     * <p>Here percentage of odd and even number are calculating
     * and print in the console.</p>
     */

    public void percentageOfOddAndEven() {
        int countEven = 0;
        float percentageOfEven;
        float percentageOfOdd;
        for (int i = 0; i < fibonacciSet.length; i++) {
            if (fibonacciSet[i] % 2 == 0) {
                countEven++;
            }
        }
        percentageOfEven = ((float) countEven / (float) sizeOfSet) * PERCENTS;
        percentageOfOdd = PERCENTS - percentageOfEven;
        System.out.println("\nPercentage of even numbers in fibonacci set: "
                + percentageOfEven + "%");
        System.out.println("Percentage of odd numbers in fibonacci set: "
                + percentageOfOdd + "%");
    }
}
